# OMS-BOT

Бот для работы с заказами и клиентами для Slack

## Техническое задание
Бот должен: 
- принимать запросы от клиентов
- забирать информацию из внешнего апи 
- и отдавать в отформатированном виде (template) все данные по клиенту (заказчик/получатель/адрес) необходимо обфусцировать  

### Первые запросы к боту
1. order_by_id - данные по заказу 
2. order_by_number - данные по заказу 
3. posting_by_id - данные по постингу
4. posting_by_number - данные по постингу


### Структура заказа
1. Шипменты (количество, ID, номер, дата создания)
2. Постинги (количество, ID, номер, дата создания)
3. Статус (oms, rezon, наименования), историю статусов, кто был инициатором, время 
4. Ссылка на админку:
stage = http://admin.oms.stg.s.o3.ru/client-order/view/1
prod = https://oms.o3.ru/client-order/view/1

### Структура постинга
1. Статус (oms, rezon), историю статусов, кто был инициатором, время 
2. таймслот (время доставки), историю переносов таймслотов 
3. delivery_type - тип доставки (пвз/курьерка/etc) 
4. склад комплектации
5. delivery_schema 
6. RezonID 
7. ClearingID 
8. события которые приходили по постингу из обменов - договорились с преподавателем не реализовывать
9. Ссылка на админку:
stage = http://admin.oms.stg.s.o3.ru/posting/view/1  
prod = https://oms.o3.ru/posting/view/1  

### Зависимости
Для бота надо предусмотреть возможность устанавливать переменными окружения внешний зависимый сервис
1. web-api 

Сейчас для теста это мок отдающий ответы по http 
`https://goschool.ozon.dev/pmozhchil/oms-web-api-mock/`

### Примеры запросов:
#### Orders
**POST** /v2/orders/by-ids  
**POST** /v2/orders/by-numbers  
`curl --location --request POST 'https://goschool.ozon.dev/pmozhchil/oms-web-api-mock/v2/orders/by-ids' \  
 --header 'Content-Type: application/json' \  
 --data-raw '{ 
   "ids": [  
     "162676548"  
   ],  
   "options": {  
     "order_options": {  
       "payment_type": true,  
       "client_loyalty_state": true,  
       "address": true,  
       "gift_sender": true,  
       "additional_info": true,  
       "application": true,  
       "marketing_actions": true,  
       "change_addressee_remaining_try_count": true  
     },  
     "shipments": true,  
     "shipment_options": {  
       "shipment_address": true,  
       "shipment_gift_sender": true,  
       "shipment_possible_states": true,  
       "change_addressee_remaining_try_count": true  
     },  
     "postings": true,  
     "posting_options": {  
       "posting_address": true,  
       "posting_gift_sender": true,  
       "posting_time_slot": true,  
       "posting_cancel_reason_src": true,  
       "posting_delivery_info": true,  
       "pvz_expiration_time": true,  
       "additional_info": true,  
       "posting_time_slot_log": true,  
       "posting_state_log": true  
     }  
   }  
 }'  
`  
#### Postrings 
**POST** /v1/postings/by-ids  
**POST** /v1/postings/by-posting-numbers  
`curl --location --request POST 'https://goschool.ozon.dev/pmozhchil/oms-web-api-mock/v1/postings/by-ids' \  
 --data-raw '{  
   "ids": ["2"],  
   "options": {  
     "posting_address": true,  
     "posting_gift_sender": true,  
     "posting_time_slot": true,  
     "posting_cancel_reason_src": true,  
     "posting_delivery_info": true,  
     "pvz_expiration_time": true,  
     "additional_info": true,  
     "posting_time_slot_log": true,  
     "posting_state_log": true  
   }  
 }'`  
 
 ### Библиотеки
В качестве базовых библиотек предлагаю взять
- https://github.com/slack-go/slack (подключение к слак)  
- https://github.com/yosssi/ace (шаблонизация ответов)  
  
 ### Требования
Исходный код решения должен быть выполнен в отдельном репозитории (форке) в вашем аккаунте.

После завершения работы над проектом: 
- создайте Merge Request в master ветку этого репозитория  
- задеплойте его в Kubernetes  

Покрытие тестами должно быть не менее 80%.