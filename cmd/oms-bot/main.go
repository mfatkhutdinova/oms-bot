package main

import (
	"context"
	slackBot "git.ozon.dev/mfatkhutdinova/oms-bot/internal/app/slack-bot"
	"git.ozon.dev/mfatkhutdinova/oms-bot/internal/config"
	"gitlab.ozon.ru/platform/scratch"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"log"
)

func main() {
	a, err := scratch.New()
	if err != nil {
		logger.Fatalf(context.Background(), "can't create app: %s", err)
	}

	slackBot, err := slackBot.New(config.GetValue(context.TODO(), config.Token).String(), config.GetValue(context.TODO(), config.Verification).String(), config.GetValue(context.TODO(), config.SigningSecret).String())
	log.Printf("Initialize slackBot")

	if err != nil {
		log.Printf("can't init slack bot: %v", err)
	}

	log.Printf("Running CommandHandler")
	a.PublicServer().HandleFunc("/{command}", slackBot.CommandHandler)

	if err := a.Run(); err != nil {
		logger.Fatalf(context.Background(), "can't run app: %s", err)
	}
}
