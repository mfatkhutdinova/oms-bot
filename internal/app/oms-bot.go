package app

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"git.ozon.dev/mfatkhutdinova/oms-bot/internal/app/structs"
)

var (
	mainUrl   = "https://goschool.ozon.dev/pmozhchil/oms-web-api-mock"
	stageLink = "http://admin.oms.stg.s.o3.ru"
	prodLink  = "https://oms.o3.ru"
)

func getPostingOptions() structs.PostingOptions {
	return structs.PostingOptions{
		PostingAddress:         true,
		PostingGiftSender:      true,
		PostingTimeSlot:        true,
		PostingCancelReasonSrc: true,
		PostingDeliveryInfo:    true,
		PvzExpirationTime:      true,
		AdditionalInfo:         true,
		PostingTimeSlotLog:     true,
	}
}

func formattingResponse(text string) string {
	responseInfoString := strings.ReplaceAll(text, ",", "\n")
	responseInfoString = strings.ReplaceAll(responseInfoString, `"`, "")
	responseInfoString = strings.ReplaceAll(responseInfoString, `:`, ": ")
	responseInfoString = strings.ReplaceAll(responseInfoString, `{`, "\n{")
	//responseInfoString = strings.ReplaceAll(responseInfoString, `}`, "\n}")
	responseInfoString = strings.ReplaceAll(responseInfoString, `{`, "")
	responseInfoString = strings.ReplaceAll(responseInfoString, `}`, "")
	responseInfoString = "```" + responseInfoString + "```"
	return responseInfoString
}

func getOrderOptions() structs.OptionsOrders {
	orderOpts := structs.OrderOptions{
		PaymentType:                      true,
		ClientLoyaltyState:               true,
		Address:                          true,
		GiftSender:                       true,
		AdditionalInfo:                   true,
		Application:                      true,
		MarketingActions:                 true,
		ChangeAddresseeRemainingTryCount: true,
	}
	shipmentOpts := structs.ShipmentOptions{
		ShipmentAddress:                  true,
		ShipmentGiftSender:               true,
		ShipmentPossibleStates:           true,
		ChangeAddresseeRemainingTryCount: true,
	}

	postingOpts := getPostingOptions()

	options := structs.OptionsOrders{
		OrderOptions:    orderOpts,
		Shipments:       true,
		ShipmentOptions: shipmentOpts,
		Postings:        true,
		PostingOptions:  postingOpts,
	}
	return options
}

func getPostingResponse(postingsInfo map[string]interface{}, slackText string) []byte {
	responseInfo, _ := json.Marshal(structs.PostingResponse{
		Oms:                   postingsInfo["client_state_id"].(float64),
		Reson:                 postingsInfo["posting_state_id"].(float64),
		Addressee:             postingsInfo["address"].(map[string]interface{})["addressee"].(string),
		TimeSloteDeliveryDate: postingsInfo["time_slot"].(map[string]interface{})["slote_delivery_date_end"].(string),
		PostingTimeSlotLog:    postingsInfo["posting_time_slot_log"].([]interface{}),
		PostingStateLog:       postingsInfo["posting_state_log"].([]interface{}),
		DeliveryType:          postingsInfo["delivery_type"].(string),
		PhysicalStoreId:       postingsInfo["physical_store_id"].(float64),
		DeliverySchema:        postingsInfo["delivery_schema"].(string),
		RezonId:               postingsInfo["rezon_id"].(float64),
		ClearingId:            postingsInfo["clearing_id"].(float64),
		StageLink:             stageLink + "/posting/view/" + slackText,
		ProdLink:              prodLink + "/posting/view/" + slackText,
	})
	return responseInfo
}

func getOrderResponse(ordersInfo map[string]interface{}, slackText string) []byte {
	var shipments []map[string]interface{}

	bs, _ := json.Marshal(ordersInfo["shipments"].(interface{}))
	sErr := json.Unmarshal(bs, &shipments)
	if sErr != nil {
		log.Println(sErr)
	}

	var postings []map[string]interface{}
	bp, _ := json.Marshal(shipments[0]["postings"].(interface{}))
	pErr := json.Unmarshal(bp, &postings)
	if pErr != nil {
		log.Println(pErr)
	}

	newPostings := []structs.PostingResponse{}
	for _, postingsInfo := range postings {
		n := structs.PostingResponse{
			Oms:                   postingsInfo["client_state_id"].(float64),
			Reson:                 postingsInfo["posting_state_id"].(float64),
			Addressee:             postingsInfo["address"].(map[string]interface{})["addressee"].(string),
			TimeSloteDeliveryDate: postingsInfo["time_slot"].(map[string]interface{})["slote_delivery_date_end"].(string),
			PostingTimeSlotLog:    postingsInfo["posting_time_slot_log"].([]interface{}),
			PostingStateLog:       postingsInfo["posting_state_log"].([]interface{}),
			DeliveryType:          postingsInfo["delivery_type"].(string),
			PhysicalStoreId:       postingsInfo["physical_store_id"].(float64),
			DeliverySchema:        postingsInfo["delivery_schema"].(string),
			RezonId:               postingsInfo["rezon_id"].(float64),
			ClearingId:            postingsInfo["clearing_id"].(float64),
			StageLink:             stageLink + "/posting/view/" + slackText,
			ProdLink:              prodLink + "/posting/view/" + slackText,
		}
		newPostings = append(newPostings, n)
	}

	responseInfo, _ := json.Marshal(structs.OrderResponse{
		Shipments: structs.ShipmentsResponse{
			Id:              shipments[0]["id"].(float64),
			ClientStateName: shipments[0]["client_state_name"].(string),
			CreatedAt:       shipments[0]["created_at"].(string),
			Number:          shipments[0]["number"].(float64),
		},
		Postings:  newPostings,
		StageLink: stageLink + "/client-order/view/" + slackText,
		ProdLink:  prodLink + "/client-order/view/" + slackText,
	})

	return responseInfo
}

func sendMessageBadRequest(w http.ResponseWriter, message string) {
	slackResponse, _ := json.Marshal(structs.SlackResponse{
		Type: "in_channel",
		Text: message,
	})
	w.Header().Add("Content-Type", "application/json")
	fmt.Fprintf(w, string(slackResponse))
	w.WriteHeader(http.StatusBadRequest)
	return
}

func sendMessageStatusOk(w http.ResponseWriter, message string) {
	slackResponse, _ := json.Marshal(structs.SlackResponse{
		Type: "in_channel",
		Text: message,
	})
	w.Header().Add("Content-Type", "application/json")
	fmt.Fprintf(w, string(slackResponse))
	w.WriteHeader(http.StatusOK)
	return
}

func PostingByIdCommand(w http.ResponseWriter, text string) (error, int) {
	orderById := append([]string{}, text)
	postBody := structs.BodyPostingById{
		Ids:     orderById,
		Options: getPostingOptions(),
	}

	postBodyByte, err := json.Marshal(postBody)
	if err != nil {
		log.Println(err)
		message := "Error from marshal"
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	client := http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	url := mainUrl + "/v1/postings/by-ids"
	//req, err := http.Post(url, "application/json", bytes.NewBuffer(postBodyByte))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(postBodyByte))

	if err != nil {
		log.Println(err)
		message := "Error from posting \n" + err.Error()
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	var resultPosting map[string][]map[string]interface{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		message := "Cannot get response"
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}
	defer resp.Body.Close()

	//if err := json.NewDecoder(req.Body).Decode(&resultPosting); err != nil {
	//	log.Println(err)
	//	message := "Cannot decode request. Try enter another id/number."
	//	sendMessageBadRequest(w, message)
	//	return errors.New(message), http.StatusBadRequest
	//}
	body, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &resultPosting); err != nil {
		log.Println(err)
		message := "Cannot decode request. Try enter another id/number."
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	postingsInfo := resultPosting["postings"][0]
	formatResponse := formattingResponse(string(getPostingResponse(postingsInfo, text)))
	sendMessageStatusOk(w, formatResponse)
	return nil, http.StatusOK
}

func PostingByNumberCommand(w http.ResponseWriter, text string) (error, int) {
	postingNumber := append([]string{}, text)
	postBody := structs.BodyPostingByNumber{
		PostingNumbers: postingNumber,
		Options:        getPostingOptions(),
	}

	postBodyByte, err := json.Marshal(postBody)
	if err != nil {
		log.Println(err)
		message := "Error from marshal"
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	client := http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	url := mainUrl + "/v1/postings/by-posting-numbers"
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(postBodyByte))
	//req, err := http.Post(url, "application/json", bytes.NewBuffer(postBodyByte))
	if err != nil {
		log.Println(err)
		message := "Error from posting \n" + err.Error()
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	var resultPosting map[string][]map[string]interface{}
	//if err := json.NewDecoder(req.Body).Decode(&resultPosting); err != nil {
	//	log.Println(err)
	//	message := "Cannot decode request. Try enter another id/number."
	//	sendMessageBadRequest(w, message)
	//	return errors.New(message), http.StatusBadRequest
	//}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		message := "Cannot get response"
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err := json.Unmarshal(body, &resultPosting); err != nil {
		log.Println(err)
		message := "Cannot decode request. Try enter another id/number."
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}
	postingsInfo := resultPosting["postings"][0]
	formatResponse := formattingResponse(string(getPostingResponse(postingsInfo, text)))
	sendMessageStatusOk(w, formatResponse)
	return nil, http.StatusOK
}

func OrderByIdCommand(w http.ResponseWriter, text string) (error, int) {
	orderId := append([]string{}, text)
	postBody := structs.BodyOrderById{
		Ids:     orderId,
		Options: getOrderOptions(),
	}

	postBodyByte, err := json.Marshal(postBody)
	if err != nil {
		log.Println(err)
		message := "Error from marshal"
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	client := http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	url := mainUrl + "/v2/orders/by-ids"
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(postBodyByte))
	//req, err := http.Post(url, "application/json", bytes.NewBuffer(postBodyByte))
	if err != nil {
		log.Println(err)
		message := "Error from posting \n" + err.Error()
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	var resultOrders map[string]map[string][]map[string]interface{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		message := "Cannot get response"
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &resultOrders); err != nil {
		log.Println(err)
		message := "Cannot decode request. Try enter another id/number."
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	//if err := json.NewDecoder(req.Body).Decode(&resultOrders); err != nil {
	//	log.Println(err)
	//	message := "Cannot decode request. Try enter another id/number."
	//	sendMessageBadRequest(w, message)
	//	return errors.New(message), http.StatusBadRequest
	//}

	ordersInfoOr, ok := resultOrders["data"]["orders"]
	if !ok {
		log.Println(err)
		message := "No information about " + text
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	ordersInfo := ordersInfoOr[0]
	formatResponse := formattingResponse(string(getOrderResponse(ordersInfo, text)))
	sendMessageStatusOk(w, formatResponse)
	return nil, http.StatusOK
}

func OrderByNumberCommand(w http.ResponseWriter, text string) (error, int) {
	orderNumber := append([]string{}, text)
	postBody := structs.BodyOrderByNumber{
		Number:  orderNumber,
		Options: getOrderOptions(),
	}

	postBodyByte, err := json.Marshal(postBody)
	if err != nil {
		log.Println(err)
		message := "Error from marshal"
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	client := http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	url := mainUrl + "/v2/orders/by-numbers"
	//req, err := http.Post(url, "application/json", bytes.NewBuffer(postBodyByte))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(postBodyByte))
	if err != nil {
		log.Println(err)
		message := "Error from posting \n" + err.Error()
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	var resultOrders map[string]map[string][]map[string]interface{}
	//if err := json.NewDecoder(req.Body).Decode(&resultOrders); err != nil {
	//	log.Println(err)
	//	message := "Cannot decode request. Try enter another id/number."
	//	sendMessageBadRequest(w, message)
	//	return errors.New(message), http.StatusBadRequest
	//}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		message := "Cannot get response"
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err := json.Unmarshal(body, &resultOrders); err != nil {
		log.Println(err)
		message := "Cannot decode request. Try enter another id/number."
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	ordersInfoOr, ok := resultOrders["data"]["orders"]
	if !ok {
		log.Println(err)
		message := "No information about " + text
		sendMessageBadRequest(w, message)
		return errors.New(message), http.StatusBadRequest
	}

	ordersInfo := ordersInfoOr[0]
	formatResponse := formattingResponse(string(getOrderResponse(ordersInfo, text)))
	sendMessageStatusOk(w, formatResponse)
	return nil, http.StatusOK
}
