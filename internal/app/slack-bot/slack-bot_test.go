package slack_bot

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"log"
	"net/http"
	"testing"

	"git.ozon.dev/mfatkhutdinova/oms-bot/internal/app"
)

func Test_OrderByIdCommand_Success(t *testing.T) {
	t.Parallel()
	a := assert.New(t)
	OrderIds := []int{
		2,
	}

	mux := http.NewServeMux()
	for _, orderId := range OrderIds {
		orderId := orderId
		name := fmt.Sprintf("orderId:%v", orderId)

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			mux.HandleFunc("/mfatk_order_by_id", func(w http.ResponseWriter, r *http.Request) {
				err, statusCode := app.OrderByIdCommand(w, string(orderId))
				log.Println(err)
				a.Equal(nil, err)
				a.Equal(200, statusCode)

			})
		})
	}
}

func Test_OrderByIdCommand_Failed(t *testing.T) {
	t.Parallel()
	a := assert.New(t)
	OrderIds := []int{
		-245,
	}

	mux := http.NewServeMux()
	for _, orderId := range OrderIds {
		orderId := orderId
		name := fmt.Sprintf("orderId:%v", orderId)

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			mux.HandleFunc("/mfatk_order_by_id", func(w http.ResponseWriter, r *http.Request) {
				err, statusCode := app.OrderByIdCommand(w, string(orderId))
				log.Println(err)
				a.NotEqual(nil, err)
				a.Equal(400, statusCode)
			})
		})
	}
}

func Test_PostingByIdCommand_Success(t *testing.T) {
	t.Parallel()
	a := assert.New(t)
	postingIds := []int{
		2,
	}

	mux := http.NewServeMux()
	for _, postingId := range postingIds {
		postingId := postingId
		name := fmt.Sprintf("PostintId:%v", postingId)

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			mux.HandleFunc("/mfatk_posting_by_id", func(w http.ResponseWriter, r *http.Request) {
				err, statusCode := app.PostingByIdCommand(w, string(postingId))
				log.Println(err)
				a.Equal(nil, err)
				a.Equal(200, statusCode)
			})
		})
	}
}

func Test_PostingByNumberCommand_Success(t *testing.T) {
	t.Parallel()
	a := assert.New(t)
	postingNumbers := []int{
		2,
	}

	mux := http.NewServeMux()
	for _, postNum := range postingNumbers {
		postNum := postNum
		name := fmt.Sprintf("PostingNumber:%v", postNum)

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			mux.HandleFunc("/mfatk_posting_by_number", func(w http.ResponseWriter, r *http.Request) {
				err, statusCode := app.PostingByNumberCommand(w, string(postNum))
				log.Println(err)
				a.Equal(nil, err)
				a.Equal(200, statusCode)
			})
		})
	}
}

func Test_OrderByNumberCommand_Failed(t *testing.T) {
	t.Parallel()
	a := assert.New(t)
	OrderNumbers := []int{
		-245,
	}

	mux := http.NewServeMux()
	for _, orderNumber := range OrderNumbers {
		orderNumber := orderNumber
		name := fmt.Sprintf("orderNumber:%v", orderNumber)

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			mux.HandleFunc("/mfatk_order_by_number", func(w http.ResponseWriter, r *http.Request) {
				err, statusCode := app.OrderByNumberCommand(w, string(orderNumber))
				log.Println(err)
				a.NotEqual(nil, err)
				a.Equal(400, statusCode)
			})
		})
	}
}
