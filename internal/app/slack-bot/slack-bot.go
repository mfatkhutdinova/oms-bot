package slack_bot

import (
	"crypto/tls"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"

	"git.ozon.dev/mfatkhutdinova/oms-bot/internal/app"
	"github.com/slack-go/slack"
)

type SlackBot struct {
	api          *slack.Client
	verification string
	secret       string
}

func New(token string, verification string, secret string) (*SlackBot, error) {
	slackAPIClient := http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	api := slack.New(token, slack.OptionHTTPClient(&slackAPIClient))
	return &SlackBot{api, verification, secret}, nil
}

func (slackbot *SlackBot) CommandHandler(w http.ResponseWriter, r *http.Request) {
	command, err := slack.SlashCommandParse(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if !command.ValidateToken(slackbot.verification) {
		//log.Printf("problem with ValidateToken")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	text := command.Text

	w.Header().Set("Content-Type", "application/json")
	switch chi.URLParam(r, "command") {
	case "mfatk_order_by_id":
		app.OrderByIdCommand(w, text)
	case "mfatk_order_by_number":
		app.OrderByNumberCommand(w, text)
	case "mfatk_posting_by_id":
		app.PostingByIdCommand(w, text)
	case "mfatk_posting_by_number":
		app.PostingByNumberCommand(w, text)
	default:
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Команда не распознана. Сформулируйте свой вопрос точнее.")
		return
	}
}
