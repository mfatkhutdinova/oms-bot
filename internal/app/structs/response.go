package structs

type PostingResponse struct {
	Oms                   float64       `json:"oms"`
	Reson                 float64       `json:"reson"`
	PostingStateLog       []interface{} `json:"posting_state_log"`
	Addressee             string        `json:"addressee"`
	TimeSloteDeliveryDate string        `json:"time_slote_delivery_date"`
	PostingTimeSlotLog    []interface{} `json:"posting_time_slot_log"`
	DeliveryType          string        `json:"delivery_type"`
	PhysicalStoreId       float64       `json:"physical_store_id"`
	DeliverySchema        string        `json:"delivery_schema"`
	RezonId               float64       `json:"rezon_id"`
	ClearingId            float64       `json:"clearing_id"`
	StageLink             string        `json:"stage_link"`
	ProdLink              string        `json:"prod_link"`
}

type ShipmentsResponse struct {
	Id              float64 `json:"id"`
	ClientStateName string  `json:"client_state_name"`
	CreatedAt       string  `json:"created_at"`
	Number          float64 `json:"number"`
}

type OrderResponse struct {
	Shipments ShipmentsResponse `json:"shipments"`
	Postings  []PostingResponse `json:"postings"`
	StageLink string            `json:"stage_link"`
	ProdLink  string            `json:"prod_link"`
}

type SlackResponse struct {
	Type string `json:"response_type"`
	Text string `json:"text"`
}
