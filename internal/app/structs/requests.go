package structs

//---------------------------Postings--------------------------------------
type PostingOptions struct {
	PostingAddress         bool `json:"posting_address"`
	PostingGiftSender      bool `json:"posting_gift_sender"`
	PostingTimeSlot        bool `json:"posting_time_slot"`
	PostingCancelReasonSrc bool `json:"posting_cancel_reason_src"`
	PostingDeliveryInfo    bool `json:"posting_delivery_info"`
	PvzExpirationTime      bool `json:"pvz_expiration_time"`
	AdditionalInfo         bool `json:"additional_info"`
	PostingTimeSlotLog     bool `json:"posting_time_slot_log"`
}

type BodyPostingById struct {
	Ids     []string       `json:"ids"`
	Options PostingOptions `json:"options"`
}

type BodyPostingByNumber struct {
	PostingNumbers []string       `json:"posting_numbers"`
	Options        PostingOptions `json:"options"`
}

//----------------------------Orders------------------------------------
type OptionsOrders struct {
	OrderOptions    OrderOptions    `json:"order_options"`
	Shipments       bool            `json:"shipments"`
	ShipmentOptions ShipmentOptions `json:"shipment_options"`
	Postings        bool            `json:"postings"`
	PostingOptions  PostingOptions  `json:"posting_options"`
}

type OrderOptions struct {
	PaymentType                      bool `json:"payment_type"`
	ClientLoyaltyState               bool `json:"client_loyalty_state"`
	Address                          bool `json:"address"`
	GiftSender                       bool `json:"gift_sender"`
	AdditionalInfo                   bool `json:"additional_info"`
	Application                      bool `json:"application"`
	MarketingActions                 bool `json:"marketing_actions"`
	ChangeAddresseeRemainingTryCount bool `json:"change_addressee_remaining_try_count"`
}

type ShipmentOptions struct {
	ShipmentAddress                  bool `json:"shipment_address"`
	ShipmentGiftSender               bool `json:"shipment_gift_sender"`
	ShipmentPossibleStates           bool `json:"shipment_possible_states"`
	ChangeAddresseeRemainingTryCount bool `json:"change_addressee_remaining_try_count"`
}

type BodyOrderById struct {
	Ids     []string      `json:"ids"`
	Options OptionsOrders `json:"options"`
}

type BodyOrderByNumber struct {
	Number  []string      `json:"number"`
	Options OptionsOrders `json:"options"`
}
