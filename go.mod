module git.ozon.dev/mfatkhutdinova/oms-bot

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/slack-go/slack v0.6.6
	github.com/stretchr/testify v1.6.1
	gitlab.ozon.ru/platform/realtime-config-go v1.8.9
	gitlab.ozon.ru/platform/scratch v1.6.12-goschool
	gitlab.ozon.ru/platform/tracer-go v1.19.0
)
